package com.lexiqa;

/**
 * Created by fotd on 11/18/16.
 */

import com.google.gson.Gson;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.filters.tmx.Parameters;
import net.sf.okapi.filters.tmx.LXQTmxFilter;
import nu.validator.htmlparser.annotation.Local;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;

public class TMXParser {

    public static String parseFile(String path) {
        int segmentCount = 0;
        ArrayList<TranslationSegment> segments = new ArrayList<>();
        try {
            LXQTmxFilter filter = new LXQTmxFilter();
            Parameters params = new Parameters();
            params.setProcessAllTargets(true);
            params.setEscapeGT(true);
            filter.setParameters(params);
            filter.open(new RawDocument(new File(path).toURI(), "UTF-8",LocaleId.EMPTY, LocaleId.EMPTY));
            LocaleId targetLocale = LocaleId.EMPTY;
            // process the input document
            while ( filter.hasNext() ) {
                Event event = filter.next();
                if ( event.getEventType() == EventType.TEXT_UNIT ) {

                    //ITextUnit tu = event.getTextUnit();
                    TextUnit tu = (TextUnit) event.getTextUnit();
                    if (targetLocale.equals(LocaleId.EMPTY)) {
                        Enumeration<LocaleId> locales = tu.getTargets();
                        if (locales.hasMoreElements()) {
                            targetLocale = locales.nextElement();
                        }
                    }

                    if ( tu.isTranslatable() ) {
                        TextContainer tcSource = tu.getSource();
                        String id =tu.getId();
                        //System.out.println("id: "+id);
                        Property prop = tu.getProperty("tuid");
                        if (prop!=null)
                            id = prop.getValue();
                        int localCount = 0;
                        for ( Segment seg : tcSource.getSegments() ) {
                            localCount++;
                            TextFragment tf = seg.getContent();
                            String coded = tf.getCodedText();
                            String decoded = tf.getText();

                            //System.out.println("source: ["+ ++segmentCount + "] coded:\n"+coded);
                            //System.out.println("decoded: " + decoded);
                            //we want the decoded string (does not contain any tag entries
                            if (decoded.trim().length()==0) {
                                coded = decoded;
                            }
                            segments.add(new TranslationSegment(decoded,"",++segmentCount +"__"+id));
                            //tf.setCodedText(tf.getCodedText());

                        }
                        if (!targetLocale.equals(LocaleId.EMPTY) && tu.hasTarget(targetLocale)) {
                            TextContainer tcTarget = tu.createTarget(targetLocale, false, IResource.COPY_SEGMENTED_CONTENT);
                            for (Segment seg : tcTarget.getSegments()) {
                                TextFragment tf = seg.getContent();
                                String coded = tf.getCodedText();
                                String decoded = tf.getText();
                                //System.out.println("target: [" + "] coded:\n"+coded);
                                //System.out.println("decoded: " + decoded);
                                if (decoded.trim().length() == 0) {
                                    coded = decoded;
                                }
                                TranslationSegment sg = segments.get(segments.size() - localCount);
                                sg.setTargetText(decoded);
                                segments.set(segments.size() - localCount, sg);
                                localCount--;
                                //tf.setCodedText(tf.getCodedText());
                            }
                        }//end if target local and there is a translation in the local
                        else {
                            //target Locale not found - this is untranslated ? missing? empty segment
                            for (int i = localCount; i > 0; i--) {
                                TranslationSegment sg = segments.get(segments.size() - i);
                                sg.setTargetText("");
                                segments.set(segments.size() - localCount, sg);
                            }
                        }
                    }
                }
            }//end while fileter has next
            XliffData data = new XliffData(filter.getSrcLang(),targetLocale,segments);
            String json = new Gson().toJson(data);

            //System.out.println(json);
            return json;
        }
        catch ( Throwable e ) {
            e.printStackTrace();
            return "";
        }
    }

    static class XliffData {
        public LocaleId sourceLocale;
        public LocaleId targetLocale;
        public ArrayList<TranslationSegment> segments;

        public XliffData(LocaleId sourceLocale, LocaleId targetLocale, ArrayList<TranslationSegment> segments) {
            this.sourceLocale = sourceLocale;
            this.targetLocale = targetLocale;
            this.segments = segments;
        }
    }

}
