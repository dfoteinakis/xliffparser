package com.lexiqa;

/**
 * Created by fotd on 11/18/16.
 */
public class TranslationSegment {

    public String getSourceText() {
        return sourceText;
    }

    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    public String getTargetText() {
        return targetText;
    }

    public void setTargetText(String targetText) {
        this.targetText = targetText;
    }

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    private String sourceText;
    private String targetText;
    private String segmentId;

    public TranslationSegment(String sourceText, String targetText, String segmentId) {
        this.sourceText = sourceText;
        this.targetText = targetText;
        this.segmentId = segmentId;
    }
}
