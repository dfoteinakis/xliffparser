package com.lexiqa;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Map;
import java.lang.reflect.Type;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.sf.okapi.common.*;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.filterwriter.XLIFFWriterParameters;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.filters.xliff.Parameters;
import net.sf.okapi.steps.common.FilterEventsWriterStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;

/**
 * Created by fotd on 11/18/16.
 */
public class XliffUpdater {

    public static int updateFile(String inputPath, String outputPath, String _srcLang, String _targetLang, String json) {

        if (inputPath==null || inputPath.length() == 0 || outputPath == null || outputPath.length() == 0 || json == null || json.length()==0) {
            return -1;
        }
        try {
            LocaleId srcLang = LocaleId.fromString(_srcLang);
            LocaleId trgLang = LocaleId.fromString(_targetLang);
            String inputEncoding = "UTF-8";
            String outputEncoding = "UTF-8";
            IFilterConfigurationMapper fcMapper;

            Type listType = new TypeToken<Map<String, ArrayList<TextContent>>>() {
            }.getType();
            Map<String, ArrayList<TextContent>> jsonToMap = new Gson().fromJson(json,listType);
            fcMapper = new FilterConfigurationMapper();
            // Fill it with the default configurations of several filters
            fcMapper.addConfigurations("net.sf.okapi.filters.xliff.XLIFFFilter");
            String ext = "xlf";

            if ( Util.isEmpty(ext) ) throw new OkapiException("No filter detected for the file extension.");
            String mimeType = MimeTypeMapper.getMimeType(ext);
            FilterConfiguration cfg = fcMapper.getDefaultConfiguration(mimeType);
            if ( cfg == null ) {
                throw new OkapiException(String.format("No configuration defined for the MIME type '%s'.", mimeType));
            }
            FilterConfiguration myCfg = fcMapper.createCustomConfiguration(cfg);
            //myCfg.loadParametersFromStream(new FileInputStream("/home/fotd/IdeaProjects/xliffParser/customXliffParams.fprm"));
            myCfg.parameters = fcMapper.getParameters(myCfg);
            fcMapper.addConfiguration(myCfg);
            IPipelineDriver driver = new PipelineDriver();

            // Add the filter step to the pipeline
            driver.addStep(new RawDocumentToFilterEventsStep());

            XLIFFSegmentHandlerStep segHandler = new XLIFFSegmentHandlerStep();
            segHandler.setMap(jsonToMap);
            driver.addStep(segHandler);

            // Add the writer step to the pipeline
            FilterEventsWriterStep fews = new FilterEventsWriterStep();
            XLIFFWriterParameters params = new XLIFFWriterParameters();
            params.setCopySource(false);
            fews.setParameters(params);
            driver.addStep(fews);


            // Set the filter configuration mapper
            driver.setFilterConfigurationMapper(fcMapper);

            driver.addBatchItem(new BatchItemContext(
                    (new File(inputPath)).toURI(), // URI of the input document
                    inputEncoding, // Default encoding
                    myCfg.configId, // Filter configuration of the document
                    (new File(outputPath)).toURI(), // Output
                    outputEncoding, // Encoding for the output
                    srcLang, // Source language
                    trgLang)); // Target language

            driver.processBatch();

        }
        catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

}
