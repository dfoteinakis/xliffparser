package com.lexiqa;

/**
 * Created by fotd on 11/18/16.
 */
import java.io.File;

import com.google.gson.Gson;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.filters.xliff.LXQXLIFFFilter;

import net.sf.okapi.filters.xliff.Parameters;
import net.sf.okapi.lib.xliff2.core.Skeleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XliffParser  {

//    private LocaleId trgLoc;

    //private int segmentCount = 0;

//    private String projectId;

//    public ArrayList<TranslationSegment> getSegments() {
//        return segments;
//    }

    //private ArrayList<TranslationSegment> segments = new ArrayList<>();

//    public void setProjectId(String projectId) {
//        this.projectId = projectId;
//    }

    public static String parseFile(String path) {
        int segmentCount = 0;
        ArrayList<TranslationSegment> segments = new ArrayList<>();
        try {
            LXQXLIFFFilter filter = new LXQXLIFFFilter();
            Parameters params = new Parameters();
            //params.setAddAltTrans(true);
            params.setEscapeGT(true);
            params.setAddAltTransGMode(true);
            filter.setParameters(params);
            filter.open(new RawDocument(new File(path).toURI(), "UTF-8", new LocaleId("it-IT"), new LocaleId("en_GB")));

            // process the input document
            while ( filter.hasNext() ) {
                Event event = filter.next();
                if ( event.getEventType() == EventType.TEXT_UNIT ) {

                    ITextUnit tu = event.getTextUnit();
                    if ( tu.isTranslatable() ) {
                        boolean isTranslated = tu.hasTarget(filter.getTargetLocale());

                        TextContainer tcTarget = tu.createTarget(filter.getTargetLocale(), false, IResource.COPY_SEGMENTED_CONTENT);
                        TextContainer tcSource = tu.getSource();
                        String id =tu.getId();
                        //if (id.equals("9db8a446-f248-41d5-abec-f8d42342b6b4")) {

                        String asdf = tu.getSkeleton().toString();

                        if (asdf.contains("locked=\"true\"")) {
                            System.out.println(id+": locked");
                            continue;
                        }
                        //}

                        //System.out.println("id: "+id);
                        int localCount = 0;
                        for ( Segment seg : tcSource.getSegments() ) {
                            localCount++;
                            TextFragment tf = seg.getContent();
                            String coded = tf.getCodedText();
                            String decoded = tf.getText();
                            String decoded2 = tf.toText();
                            List<Code> codes =  tf.getCodes();
                            for (Code code: codes) {
                                //String s = code.getDisplayText();
                                String outerData = code.getOuterData();
                                //String data = code.getData();
                                //decoded2 = decoded2.replaceFirst(data,outerData);
                                String tagType = code.getTagType().name();
                                String encodedTag = "";
                                if (tagType.equals("OPENING")) {
                                    encodedTag+= (char)TextFragment.MARKER_OPENING;
                                    int codeStart = tf.getIndex(code.getId());
                                    encodedTag+= TextFragment.toChar(codeStart);
                                }
                                else if (tagType.equals("CLOSING")) {
                                    encodedTag+= (char)TextFragment.MARKER_CLOSING;
                                    int codeEnd = tf.getIndexForClosing(code.getId());
                                    encodedTag+= TextFragment.toChar(codeEnd);
                                }
                                else { //ISOLATED
                                    encodedTag+= (char)TextFragment.MARKER_ISOLATED;
                                    int codeStart = tf.getIndex(code.getId());
                                    encodedTag+= TextFragment.toChar(codeStart);
                                }
                                coded = coded.replaceFirst(encodedTag, outerData);
                            }
                            //System.out.println("source: ["+ ++segmentCount + "] coded:\n"+coded);
                            //System.out.println("decoded: " + decoded);
                            //we want the decoded string (does not contain any tag entries
                            if (decoded.trim().length()==0) {
                                coded = decoded;
                            }
                            segments.add(new TranslationSegment(coded,"",segmentCount++ +"__"+id));
                            //tf.setCodedText(tf.getCodedText());

                        }
                        for ( Segment seg : tcTarget.getSegments() ) {
                            String coded = "";
                            if (isTranslated) {
                                TextFragment tf = seg.getContent();
                                coded = tf.getCodedText();
                                String decoded = tf.getText();
                                //System.out.println("target: [" + "] coded:\n"+coded);
                                //System.out.println("decoded: " + decoded);
                                String decoded2 = tf.toText();
                                List<Code> codes = tf.getCodes();
                                for (Code code : codes) {
                                    String outerData = code.getOuterData();
                                    //String data = code.getData();
                                    //decoded2 = decoded2.replaceFirst(data,outerData);
                                    String tagType = code.getTagType().name();
                                    String encodedTag = "";
                                    if (tagType.equals("OPENING")) {
                                        encodedTag += (char) TextFragment.MARKER_OPENING;
                                        int codeStart = tf.getIndex(code.getId());
                                        encodedTag += TextFragment.toChar(codeStart);
                                    } else if (tagType.equals("CLOSING")) {
                                        encodedTag += (char) TextFragment.MARKER_CLOSING;
                                        int codeEnd = tf.getIndexForClosing(code.getId());
                                        encodedTag += TextFragment.toChar(codeEnd);
                                    } else { //ISOLATED
                                        encodedTag += (char) TextFragment.MARKER_ISOLATED;
                                        int codeStart = tf.getIndex(code.getId());
                                        encodedTag += TextFragment.toChar(codeStart);
                                    }
                                    coded = coded.replaceFirst(encodedTag, outerData);
                                }
                                if (decoded.trim().length() == 0) {
                                    coded = decoded;
                                }
                            }
                            TranslationSegment sg = segments.get(segments.size()-localCount);
                            sg.setTargetText(coded);
                            segments.set(segments.size()-localCount,sg);
                            localCount--;
                            //tf.setCodedText(tf.getCodedText());
                        }
                    }
                }
            }//end while fileter has next
            XliffData data = new XliffData(filter.getSourceLocale(),filter.getTargetLocale(),segments);
            /*
            Map<String, ArrayList<TextContent>> map = new HashMap<String, ArrayList<TextContent>>();
            for (int i = 0; i < segments.size(); i++) {
                TranslationSegment ts = (TranslationSegment)segments.get(i);
                String id = ts.getSegmentId().split("__")[1];
                TextContent tc =new TextContent();
                tc.sourceText = ts.getSourceText();
                tc.targetText =ts.getTargetText();
                if (tc.sourceText.length()>0) {
                    if (map.containsKey(id)) {
                        ArrayList<TextContent> ar = map.get(id);
                        ar.add(tc);
                    } else {
                        ArrayList<TextContent> ar = new ArrayList<TextContent>(1);
                        ar.add(tc);
                    }
                }

            }
            */
            String json = new Gson().toJson(data);
            //String json = new Gson().toJson(map);
            //System.out.println(json);
            return json;
        }
        catch ( Throwable e ) {
            e.printStackTrace();
            return "";
        }
    }

    static class XliffData {
        public LocaleId sourceLocale;
        public LocaleId targetLocale;
        public ArrayList<TranslationSegment> segments;

        public XliffData(LocaleId sourceLocale, LocaleId targetLocale, ArrayList<TranslationSegment> segments) {
            this.sourceLocale = sourceLocale;
            this.targetLocale = targetLocale;
            this.segments = segments;
        }
    }

}
