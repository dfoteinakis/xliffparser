package com.lexiqa;

/**
 * Created by fotd on 11/18/16.
 */
import net.sf.okapi.common.Event;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.*;

import java.util.*;

public class XLIFFSegmentHandlerStep extends BasePipelineStep {

    private LocaleId trgLoc;
    private Map<String, ArrayList<TextContent>> map;
    @SuppressWarnings("deprecation")
    @StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
    public void setTargetLocale (LocaleId targetLocale) {
        trgLoc = targetLocale;
    }

    public String getName () {
        return "update";
    }

    public String getDescription () {
        return "Converts text units content to upper cases.";
    }

    public void setMap (Map<String, ArrayList<TextContent>> map) {
        this.map = map;
    }

    @Override
    protected Event handleTextUnit (Event event) {
        ITextUnit tu = event.getTextUnit();
        int count = 0;
        if ( tu.isTranslatable() ) {
            String id =tu.getId();
            //System.out.println("approved there? "+tu.hasProperty("approved"));

            ArrayList<TextContent> list =  map.get(id);
//            System.out.println("approved there? "+tu.a);

            if (list != null) {
                Collections.sort(list, new CustomComparator());
                int max = list.size();
                TextContainer tc = tu.createTarget(trgLoc, false, IResource.COPY_SEGMENTED_CONTENT);
                //TextContainer tc = tu.createTarget(trgLoc, false, IResource.COPY_ALL);
                for (Segment seg : tc.getSegments()) {
                    if (count < max) {
                        TextFragment tf = seg.getContent();
                        String text = list.get(count++).targetText;
                        if (tf.hasCode()) {
                            List<Code> codes =  tf.getCodes();
                            for (Code code: codes) {
                                String outerData = code.getOuterData();
                                String tagType = code.getTagType().name();
                                String encodedTag = "";
                                if (tagType.equals("OPENING")) {
                                    encodedTag+= (char)TextFragment.MARKER_OPENING;
                                    int codeStart = tf.getIndex(code.getId());
                                    encodedTag+= TextFragment.toChar(codeStart);
                                }
                                else if (tagType.equals("CLOSING")) {
                                    encodedTag+= (char)TextFragment.MARKER_CLOSING;
                                    int codeEnd = tf.getIndexForClosing(code.getId());
                                    encodedTag+= TextFragment.toChar(codeEnd);
                                }
                                else { //ISOLATED
                                    encodedTag+= (char)TextFragment.MARKER_ISOLATED;
                                    int codeStart = tf.getIndex(code.getId());
                                    encodedTag+= TextFragment.toChar(codeStart);
                                }
                                text = text.replaceFirst(outerData,encodedTag);
                            }
                        }
                        tf.setCodedText(text);
                    }
                }
            }
        }

        return event;
    }

    class CustomComparator implements Comparator<TextContent> {
        @Override
        public int compare(TextContent o1, TextContent o2) {
            return o1.getIndx() - o2.getIndx();
        }
    }
}
