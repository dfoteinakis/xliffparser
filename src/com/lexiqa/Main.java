package com.lexiqa;

import java.io.File;
import java.util.ArrayList;

import com.google.gson.Gson;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.MimeTypeMapper;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.common.filters.FilterConfiguration;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilterConfigurationMapper;
import net.sf.okapi.common.pipelinedriver.BatchItemContext;
import net.sf.okapi.common.pipelinedriver.IPipelineDriver;
import net.sf.okapi.common.pipelinedriver.PipelineDriver;
import net.sf.okapi.steps.common.FilterEventsWriterStep;
import net.sf.okapi.steps.common.RawDocumentToFilterEventsStep;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    private static LocaleId srcLang = LocaleId.fromString("en");
    private static LocaleId trgLang = LocaleId.fromString("fr");
    private static String inputEncoding = "UTF-8";
    private static String outputEncoding = "UTF-8";
    private static String inputPath = null;
    private static String outputPath = null;
    private static String steps = "";
    private static IFilterConfigurationMapper fcMapper;

    private static String jsonInput = "";
    public static void main (String[] args) {
        try {
            System.out.println("------------------------------------------------------------");
            if ( args.length == 0 ) {
                return;
            }
            // Get the parameters
            for ( int i=0; i<args.length; i++ ) {
                if ( args[i].equals("-sl") ) srcLang = LocaleId.fromString(args[++i]);
                else if ( args[i].equals("-tl") ) trgLang = LocaleId.fromString(args[++i]);
                else if ( args[i].equals("-ie") ) inputEncoding = args[++i];
                else if ( args[i].equals("-oe") ) outputEncoding = args[++i];
                else if ( inputPath == null ) inputPath = args[i];
                else outputPath = args[i];
            }

            if ( inputPath == null ) {
                throw new OkapiException("No input file defined.");
            }
            //parseXlfToJson(inputPath);
            //jsonInput =  XliffParser.parseFile((inputPath));
            //System.out.println("malalala");
            //jsonInput = "{\"154\":[{\"indx\":\"0\",\"sourceText\":\"This is a dog.\",\"targetText\":\"Koulourakia\"}]}";
            //jsonInput = new String(Files.readAllBytes(Paths.get("test.json")));

            //XliffUpdater.updateFile("localexTest.xliff","localexTest_out.xliff","en-US","tr-TR",jsonInput);
            XliffParser.parseFile(inputPath);
           //TMXParser.parseFile((inputPath));

        }
        catch ( Throwable e ) {
            e.printStackTrace();
        }
    }
/*
    public static String parseXlfToJson(String path) {
        // Create the mapper
        fcMapper = new FilterConfigurationMapper();
        // Fill it with the default configurations of several filters
        fcMapper.addConfigurations("net.sf.okapi.filters.xliff.XLIFFFilter");

        // Detect the file to use
        String ext = "xlf";

        if ( Util.isEmpty(ext) ) throw new OkapiException("No filter detected for the file extension.");
        String mimeType = MimeTypeMapper.getMimeType(ext);
        FilterConfiguration cfg = fcMapper.getDefaultConfiguration(mimeType);
        if ( cfg == null ) {
            throw new OkapiException(String.format("No configuration defined for the MIME type '%s'.", mimeType));
        }
        // Process
        return pipeline1(cfg);
    }

    private static String pipeline1 (FilterConfiguration config) {
        // Create the driver
        IPipelineDriver driver = new PipelineDriver();

        // Add the filter step to the pipeline
        driver.addStep(new RawDocumentToFilterEventsStep());

        SegmentHandlerStep segHandler = new SegmentHandlerStep();
        driver.addStep(segHandler);

        // Add the writer step to the pipeline
        //driver.addStep(new FilterEventsWriterStep());

        // Set the filter configuration mapper
        driver.setFilterConfigurationMapper(fcMapper);

        driver.addBatchItem(new BatchItemContext(
                (new File(inputPath)).toURI(), // URI of the input document
                inputEncoding, // Default encoding
                config.configId, // Filter configuration of the document
                (new File(outputPath)).toURI(), // Output
                outputEncoding, // Encoding for the output
                srcLang, // Source language
                trgLang)); // Target language

        driver.processBatch();


        String json = new Gson().toJson(segments);

        System.out.println(json);
        return json;
    }
*/
}
