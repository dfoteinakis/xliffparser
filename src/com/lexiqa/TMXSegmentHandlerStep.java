package com.lexiqa;

/**
 * Created by fotd on 11/18/16.
 */
import net.sf.okapi.common.Event;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

public class TMXSegmentHandlerStep extends BasePipelineStep {

    private LocaleId trgLoc;
    private Map<String, ArrayList<TextContent>> map;
    @SuppressWarnings("deprecation")
    @StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
    public void setTargetLocale (LocaleId targetLocale) {
        trgLoc = targetLocale;
    }

    public String getName () {
        return "update";
    }

    public String getDescription () {
        return "Converts text units content to upper cases.";
    }

    public void setMap (Map<String, ArrayList<TextContent>> map) {
        this.map = map;
    }

    @Override
    protected Event handleTextUnit (Event event) {
        ITextUnit tu = event.getTextUnit();
        int count = 0;
        if ( tu.isTranslatable() ) {
            String id =tu.getId();
            Property prop = tu.getProperty("tuid");
            if (prop!=null)
                id = prop.getValue();
            ArrayList<TextContent> list =  map.get(id);
            if (list != null) {
                Collections.sort(list, new CustomComparator());
                int max = list.size();
                TextContainer tc = tu.createTarget(trgLoc, false, IResource.COPY_SEGMENTED_CONTENT);
                for (Segment seg : tc.getSegments()) {
                    if (count < max) {
                        TextFragment tf = seg.getContent();
                        String text = list.get(count++).targetText;
                        tf.setCodedText(text);
                    }
                }
            }
        }
        return event;
    }

    class CustomComparator implements Comparator<TextContent> {
        @Override
        public int compare(TextContent o1, TextContent o2) {
            return o1.getIndx() - o2.getIndx();
        }
    }
}
